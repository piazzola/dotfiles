# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

#if [ "$color_prompt" = yes ]; then
    PS1='\[\033[38;5;226m\]\A\[\033[00m\] \[\033[38;5;049m\]\u\[\033[00m\] \[\033[38;5;45m\][\w]\[\033[38;5;207m\]$(__git_ps1)\n\[\033[38;5;196m\]->\[\033[00m\] '
#fi

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# enable bash completion in interactive shells
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

export PROMPT_COMMAND='echo -en "\033]0; $(basename `pwd`) \a"'

PATH=${HOME}/Documents/bin:${HOME}/.local/bin/:$PATH
source ~/.git-prompt.sh

if [ $commands[kubectl] ]; then
 source <(kubectl completion bash)
fi

#if [ $commands[heketi] ]; then
# source <(heketi completion bash)
#fi

#if [ $commands[minikube] ]; then
# source <(minikube completion bash)
#fi

export GOROOT=/usr/local/go
export GOPATH=~/go
export PATH=$GOROOT/bin:$PATH
