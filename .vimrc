execute pathogen#infect()

set splitbelow 
set splitright
set syntax=on
set nocp
set tabstop=4
set shiftwidth=4
set expandtab
set showmatch
set hlsearch
set background=dark
set hidden

nnoremap <TAB> :bn<CR>
map <C-n> :NERDTreeToggle<CR>
autocmd FileType python noremap <buffer> <F8> :call Autopep8()<CR>

"let g:solarized_termtrans = 1
colorscheme solarized

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#whitespace#enabled = 0
let g:airline_theme="solarized"

call plug#begin('~/.vim/plugged')
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'
Plug 'nvie/vim-flake8'
Plug 'tell-k/vim-autopep8'
Plug 'preservim/nerdtree'
call plug#end()

let g:deoplete#enable_at_startup = 1
let g:autopep8_disable_show_diff=1
