autoload -Uz compinit promptinit
compinit 
#compaudit | xargs chmod g-w
promptinit
autoload -U colors; colors

HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000

# Appends every command to the history file once it is executed
setopt inc_append_history
# Reloads the history whenever you use it
setopt share_history

zstyle ':completion:*' special-dirs true

source ~/git-prompt.sh
source ~/kubectl.zsh
source <(kubectl completion zsh)
source ~/.aliases

#precmd_functions+=set_prompt
precmd () {
#PROMPT="%F{white}%T %n@%m [%~]$(__git_ps1) %%%f "
PROMPT="%F{white}%T %~$(__git_ps1) >%f "
RPROMPT="%{$fg[blue]%}($ZSH_KUBECTL_PROMPT)%{$reset_color%}"
echo -ne "\033]0;$PWD\007"
}

export ANDROID_SDK_ROOT=/Users/marco/Library/Android/sdk

PATH=$PATH:/home/marco/.yarn/bin
. /etc/profile.d/vte.sh

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/pulse/extra/usr/lib/x86_64-linux-gnu/

